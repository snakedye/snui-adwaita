use snui::{environment::*, widgets::*, *};
use std::cell::Cell;
use std::rc::Rc;

/// A tab in [`TabBar`].
pub struct InnerTab<W> {
    inner: W,
    opened: Rc<Cell<bool>>,
}

impl<W> InnerTab<W> {
    pub fn new<F>(f: F) -> Self
    where
        F: FnOnce(Rc<Cell<bool>>) -> W,
    {
        let opened = Rc::new(Cell::new(true));
        Self {
            inner: f(opened.clone()),
            opened,
        }
    }
}

impl<T> InnerTab<Box<dyn Widget<T>>> {
    pub fn dyn_new<F, W>(f: F) -> Self
    where
        W: Widget<T> + 'static,
        F: FnOnce(Rc<Cell<bool>>) -> W,
    {
        let opened = Rc::new(Cell::new(true));
        Self {
            inner: Box::new(f(opened.clone())),
            opened,
        }
    }
}

impl<T, W: Widget<T>> Widget<T> for InnerTab<W> {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        self.inner.draw_scene(scene, env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.inner.event(ctx, event, env)
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.inner.update(ctx, env)
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        self.inner.layout(ctx, bc, env)
    }
}

/// A list of tabs.
pub struct TabBar<T, F, W: Widget<T> = Box<dyn Widget<T>>>
where
    F: FnMut(&mut UpdateContext<T>, &Env) -> Option<InnerTab<W>>,
{
    f: F,
    tabs: Vec<Positioner<Proxy<T, InnerTab<W>>>>,
}

impl<T, F> TabBar<T, F, Box<dyn Widget<T>>>
where
    F: FnMut(&mut UpdateContext<T>, &Env) -> Option<InnerTab<Box<dyn Widget<T>>>>,
{
    pub fn default(f: F) -> Self {
        Self {
            f,
            tabs: Vec::new(),
        }
    }
}

impl<T, F, W: Widget<T>> TabBar<T, F, W>
where
    F: FnMut(&mut UpdateContext<T>, &Env) -> Option<InnerTab<W>>,
{
    pub fn new(f: F) -> Self {
        Self {
            f,
            tabs: Vec::new(),
        }
    }
    pub fn add<Tab>(&mut self, f: Tab)
    where
        Tab: FnOnce(Rc<Cell<bool>>) -> W,
    {
        self.tabs.push(child(InnerTab::new(f)));
    }
    pub fn with<Tab>(mut self, f: Tab) -> Self
    where
        Tab: FnOnce(Rc<Cell<bool>>) -> W,
    {
        self.add(f);
        self
    }
}

impl<T, W: Widget<T>, F> Widget<T> for TabBar<T, F, W>
where
    F: FnMut(&mut UpdateContext<T>, &Env) -> Option<InnerTab<W>>,
{
    fn draw_scene(&mut self, mut scene: scene::Scene, env: &Env) {
        for tab in &mut self.tabs {
            tab.draw_scene(scene.borrow(), env)
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        let mut damage = Damage::None;
        self.tabs.retain_mut(|tab| {
            damage = damage.max(tab.event(ctx, event, env));
            if !tab.opened.get() {
                damage = damage.max(Damage::Partial);
            }
            tab.opened.get()
        });
        damage
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        ctx.prepare();
        if let Some(mut tab) = (self.f)(ctx, env) {
            tab.event(ctx, STYLESHEET_SIGNAL, env);
            self.tabs.push(child(tab));
        }
        let mut damage = Damage::None;
        self.tabs.retain_mut(|tab| {
            damage = damage.max(tab.update(ctx, env));
            if !tab.opened.get() {
                damage = damage.max(Damage::Partial);
            }
            tab.opened.get()
        });
        damage
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        let len = self.tabs.len();
        ColumnIter::new(self.tabs.iter_mut(), ctx, bc, env)
            .with_len(len)
            .reduce(|acc, region| acc.merge(&region))
            .unwrap_or_default()
            .size()
    }
}
