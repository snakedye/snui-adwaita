use snui::environment::*;
use snui::{scene::Texture, tiny_skia::Transform, widgets::*, *};
use snui_derive::{guard, Guarded};
use std::{f32::consts::SQRT_2, ops::DerefMut};

/// A close icon.
#[derive(Guarded)]
pub struct Close {
    line: Rectangle,
}

impl Default for Close {
    fn default() -> Self {
        Self {
            line: Rectangle::new(2., 0.).radius(RelativeUnit::Percent(50.)),
        }
    }
}

impl Close {
    pub fn new<T>() -> impl Widget<T> + Themeable {
        Canvas::from(Close::default())
            .class("close")
            .style("icon")
            .class("pill")
    }
}

impl Geometry for Close {
    fn width(&self) -> f32 {
        self.line.height() / SQRT_2
    }
    fn height(&self) -> f32 {
        self.line.height() / SQRT_2
    }
}

impl GeometryExt for Close {
    fn set_width(&mut self, _: f32) {
        self.line.set_width(2.)
    }
    fn set_height(&mut self, height: f32) {
        self.line.set_height(height * SQRT_2)
    }
    fn set_size(&mut self, width: f32, height: f32) {
        self.line.set_height(width.min(height) * SQRT_2)
    }
}

impl Themeable for Close {
    const NAME: &'static str = "icon";

    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env.try_get::<Texture>(list, "color") {
            self.line.set_texture(texture)
        }
    }
}

impl Drawable for Close {
    fn draw(&self, ctx: &mut DrawContext, mut transform: Transform) {
        transform = transform.pre_translate(
            (self.width() - self.line.width()) / 2.,
            (self.height() - self.line.height()) / 2.,
        );
        let x = self.line.width() / 2.;
        let y = self.line.height() / 2.;
        self.line.draw(
            ctx,
            Transform::from_rotate_at(45., x, y).post_concat(transform),
        );
        self.line.draw(
            ctx,
            Transform::from_rotate_at(-45., x, y).post_concat(transform),
        )
    }
}

/// Three dot icon.
#[derive(Guarded)]
pub struct Menu {
    circle: Circle,
}

impl Default for Menu {
    fn default() -> Self {
        Self {
            circle: Circle::new(1.),
        }
    }
}

impl Themeable for Menu {
    const NAME: &'static str = "icon";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env.try_get::<Texture>(list, "color") {
            self.circle.set_texture(texture)
        }
    }
}

impl Geometry for Menu {
    fn width(&self) -> f32 {
        self.circle.width() * 5.
    }
    fn height(&self) -> f32 {
        self.circle.height() * 5.
    }
}

impl Drawable for Menu {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        let dx = self.circle.width() * 2.;
        self.circle.draw(ctx, transform.pre_translate(dx, 0.));
        self.circle
            .draw(ctx, transform.pre_translate(dx, 2. * self.circle.height()));
        self.circle
            .draw(ctx, transform.pre_translate(dx, 4. * self.circle.height()));
    }
}

impl GeometryExt for Menu {
    fn set_width(&mut self, _width: f32) {}
    fn set_height(&mut self, height: f32) {
        self.circle.set_radius(height / 10.);
    }
}

/// Three bar icon.
#[derive(Guarded, Default, Clone)]
pub struct Options {
    rect: Rectangle,
}

impl Themeable for Options {
    const NAME: &'static str = "icon";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env.try_get::<Texture>(list, "color") {
            self.rect.set_texture(texture)
        }
    }
}

impl Geometry for Options {
    fn width(&self) -> f32 {
        self.rect.width()
    }
    fn height(&self) -> f32 {
        self.rect.height() * 5.
    }
}

impl Drawable for Options {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform) {
        self.rect.draw(ctx, transform);
        self.rect
            .draw(ctx, transform.pre_translate(0., 2. * self.rect.height()));
        self.rect
            .draw(ctx, transform.pre_translate(0., 4. * self.rect.height()));
    }
}

impl GeometryExt for Options {
    fn set_width(&mut self, width: f32) {
        self.rect.set_width(width)
    }
    fn set_height(&mut self, height: f32) {
        self.rect.set_height(height / 5.)
    }
}

#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
    Right = 45,
    DownRight = 90,
    Down = 135,
    DownLeft = 180,
    Left = -135,
    UpLeft = -90,
    Up = -45,
    UpRight = 0,
}

#[guard]
#[derive(PartialEq, Clone, Debug)]
pub struct Arrow {
    angle: f32,
    size: f32,
    texture: Texture,
}

impl Arrow {
    pub fn new(direction: Direction) -> Canvas<Self> {
        Canvas::new(Self {
            angle: Guard::from(direction as i32 as f32),
            size: Guard::from(0.),
            texture: Guard::from(Texture::default()),
        })
    }
    pub fn from_angle(angle: f32) -> Canvas<Self> {
        Canvas::new(Self {
            angle: Guard::from(angle),
            size: Guard::from(0.),
            texture: Guard::from(Texture::default()),
        })
    }
    pub fn set_angle(&mut self, angle: f32) {
        self.angle.set(angle);
    }
}

impl Themeable for Arrow {
    const NAME: &'static str = "icon";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(texture) = env.try_get::<Texture>(list, "color") {
            self.texture.set(texture);
        }
    }
}

impl Geometry for Arrow {
    fn width(&self) -> f32 {
        (self.size.get() / SQRT_2) * 2.
    }
    fn height(&self) -> f32 {
        (self.size.get() / SQRT_2) * 2.
    }
}

impl Drawable for Arrow {
    fn draw(&self, ctx: &mut DrawContext, mut transform: Transform) {
        ctx.draw(|ctx, mut pb| {
            pb.line_to(self.size.get(), 0.);
            pb.line_to(self.size.get(), self.size.get());

            let path = pb.finish().unwrap();

            let dx = if self.angle.is_sign_negative() {
                self.width() / 4.
            } else {
                0.
            };

            transform = Transform::from_rotate_at(
                self.angle.get(),
                self.size.get() / 2.,
                self.size.get() / 2.,
            )
            .post_concat(transform.pre_translate(dx, (self.height() - self.size.get()) / 2.));

            stroke_path(&path, &self.texture, 2., ctx.deref_mut(), self, transform);

            path
        });
    }
}

impl GeometryExt for Arrow {
    fn set_width(&mut self, _width: f32) {}
    fn set_height(&mut self, height: f32) {
        self.size.set(height * SQRT_2)
    }
    fn set_size(&mut self, width: f32, height: f32) {
        self.size.set(width.min(height) * SQRT_2)
    }
}
