use snui::environment::*;
use snui::{scene::Texture, widgets::*, *};

/// An item in a vertical boxed list.
pub struct RowItem<W> {
    item: W,
    border: Rectangle,
}

impl<W> RowItem<W> {
    pub fn new(item: W) -> Self {
        Self {
            item,
            border: Rectangle::new(0., 1.),
        }
    }
}

impl<T, W: Widget<T>> Widget<T> for RowItem<W> {
    fn draw_scene(&mut self, mut scene: scene::Scene, env: &Env) {
        self.border.restore();
        scene.borrow().draw(&self.border);
        self.item
            .draw_scene(scene.translate(0., self.border.height()), env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.item.event(ctx, event, env).max(self.border.damage())
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.item.update(ctx, env).max(self.border.damage())
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        env.push(self.border.id(Some("linked")))
            .map(&mut self.border);
        self.border.set_width(bc.maximum_width());
        let Size { width, height } = self.item.layout(ctx, bc, env);
        Size::new(width, height + self.border.height())
    }
}

impl<W: Themeable> Themeable for RowItem<W> {
    const NAME: &'static str = "list-item";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if list.iter().filter_map(|id| id.class()).any(|c| c == "top") {
            self.border.set_height(0.);
        }
        if let Some(texture) = env.try_get::<Texture>(list.push(self.id(Some("linked"))), "color") {
            self.border.set_texture(texture)
        }
        self.item.theme(list, env);
    }
}

/// An item in a horizontal boxed list.
pub struct ColumnItem<W> {
    item: W,
    border: Rectangle,
}

impl<W> ColumnItem<W> {
    pub fn new(item: W) -> Self {
        Self {
            item,
            border: Rectangle::new(1., 0.),
        }
    }
}

impl<T, W: Widget<T>> Widget<T> for ColumnItem<W> {
    fn draw_scene(&mut self, mut scene: scene::Scene, env: &Env) {
        self.border.restore();
        scene.borrow().draw(&self.border);
        self.item
            .draw_scene(scene.translate(self.border.width(), 0.), env)
    }
    fn event(&mut self, ctx: &mut UpdateContext<T>, event: Event, env: &Env) -> Damage {
        self.item.event(ctx, event, env).max(self.border.damage())
    }
    fn update(&mut self, ctx: &mut UpdateContext<T>, env: &Env) -> Damage {
        self.item.update(ctx, env).max(self.border.damage())
    }
    fn layout(&mut self, ctx: &mut LayoutContext<T>, bc: &BoxConstraints, env: &Env) -> Size {
        env.map(&mut self.border);
        self.border.set_height(bc.maximum_height());
        let Size { width, height } = self.item.layout(ctx, bc, env);
        Size::new(width + self.border.width(), height)
    }
}

impl<W: Themeable> Themeable for ColumnItem<W> {
    const NAME: &'static str = "list-item";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if list.iter().filter_map(|id| id.class()).any(|c| c == "left") {
            self.border.set_width(0.);
        }
        if let Some(texture) = env.try_get::<Texture>(list.push(self.id(Some("linked"))), "color") {
            self.border.set_texture(texture)
        }
        self.item.theme(list, env);
    }
}
