pub mod adwaita;
mod config;
pub mod widgets;

pub use config::load_theme;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
