//! Widgets attempting to follow the GNOME's Human Interface Guidelines.

pub mod icons;
pub mod list;
pub mod tab;

use std::cell::Cell;
use std::rc::Rc;

use icons::Close;
use snui::environment::*;
use snui::msg_api::*;
use snui::{utils::*, widgets::*, *};
use std::ops::DerefMut;
use tab::TabBar;

use self::tab::InnerTab;

#[macro_export]
/// Create custom widgets.
macro_rules! widget {
    (
        $(#[$meta:meta])*
        $type:ident { $($func:item)* }
    ) => {
        $(#[$meta])*
        pub struct $type;

        impl $type {
            $($func)*
        }
    };
}

pub enum ViewOperation<'a, T> {
    Remove(&'a T),
    Select(&'a T),
}

#[macro_export]
/// Create buttons with custom classes.
macro_rules! AdwButtonBuilder {
    ($type:ty: $label:expr, $f:expr, $($class:literal)|*) => {
        WidgetStyle::new(
            Expander::vertical(
                Label::from($label).class("title-3")
            )
        )
        $(
            .class($class)
        )*
        .class("button")
        .button(move |this, ctx: &mut UpdateContext<$type>, env, p| match p {
            Pointer::Leave | Pointer::Enter => {
                env.map(this);
            }
            _ => {
                if p.left_button_click().is_some() {
                    $f(ctx, env)
                }
            }
        })
    };
}

widget! {
    /// A widget that trigger a callback on a left click.
    AdwButton {
        /// A standard button.
        pub fn new<T, F>(label: &str, mut f: F) -> impl Widget<T> + Themeable
        where
            F: FnMut(&mut UpdateContext<T>, &Env)
        {
            AdwButtonBuilder!(T: label, f, "button")
        }
        /// A flat button.
        pub fn flat<T, F>(label: &str, mut f: F) -> impl Widget<T> + Themeable
        where
            F: FnMut(&mut UpdateContext<T>, &Env)
        {
            AdwButtonBuilder!(T: label, f, "flat")
        }
        /// A button with an accent color.
        pub fn accent<T, F>(label: &str, mut f: F) -> impl Widget<T> + Themeable
        where
            F: FnMut(&mut UpdateContext<T>, &Env)
        {
            AdwButtonBuilder!(T: label, f, "accent")
        }
        /// A button with the destructive color.
        pub fn destructive<T, F>(label: &str, mut f: F) -> impl Widget<T> + Themeable
        where
            F: FnMut(&mut UpdateContext<T>, &Env)
        {
            AdwButtonBuilder!(T: label, f, "destructive-action")
        }
        /// A button with a warning.
        pub fn warning<T, F>(label: &str, mut f: F) -> impl Widget<T> + Themeable
        where
            F: FnMut(&mut UpdateContext<T>, &Env)
        {
            AdwButtonBuilder!(T: label, f, "warning")
        }
    }
}

widget! {
    /// A widget that hold a single line text field.
    AdwInput {
        /// An input field.
        pub fn new<M, T>(message: M) -> impl Widget<T> + Themeable
        where
            for<'a> M::Message<'a>: PartialEq + FieldParameter + AsRef<str>,
            for<'a> M: MessageBuilder<Data<'a> = &'a str>,
            for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
        {
            Input::new(message)
            	.with_min_height(20.) 
                .style("view")
                .class("rounded")
                .class("frame")
        }
    }
}

widget! {
    /// An interactive progress bar with an handle.
    AdwSlider {
        pub fn new<M, T>(message: M) -> impl Widget<T> + Themeable
        where
            for<'a> M: MessageBuilder<Data<'a> = f32>,
            for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
            for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<f32>,
        {
            Slider::new(message)
                .class("accent")
                .class("pill")
                .with_fixed_height(10.)
                .anchor(START, CENTER)
        }
    }
}

widget! {
    /// A progress bar.
    AdwProgress {
        pub fn new<M, T>(message: M) -> impl Widget<T> + Themeable
        where
            for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<f32>,
            for<'a> M: MessageBuilder<Data<'a> = f32>,
            for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
        {
            Progress::new(message)
                .class("accent")
                .class("pill")
                .flex(Orientation::Horizontal)
                .alignment(START)
                .style("progress")
                .class("pill")
                .with_fixed_height(6.)
        }
    }
}

widget! {
    /// An on and off toggle.
    AdwToggle {
        pub fn new<M: Clone, T>(message: M) -> impl Widget<T> + Themeable
        where
            for<'a> M::Message<'a>: PartialEq + FieldParameter + Release<bool>,
            for<'a> M: MessageBuilder<Data<'a> = bool>,
            for<'a> T: Request<M> + MessageHandler<M::Message<'a>>,
        {
            Toggle::<_, Quadratic>::new(message.clone())
                .duration(100)
                .with_size(44., 22.)
                .class("pill")
                .style("toggle")
                .class("pill")
                .activate(message, |this, active, _, env| {
                    match active {
                        true => {
                            this.deref_mut().set_class("accent")
                        }
                        false => {
                            this.deref_mut().set_class("toggle")
                        }
                    }
                    env.map(this)
                })
        }
    }
}

fn tab<T, M>(cell: Rc<Cell<bool>>, label: &str, key: M) -> impl Widget<T>
where
    for<'m> M::Message<'m>: Release<bool>,
    for<'m> T: 'static + Request<M> + MessageHandler<ViewOperation<'m, M>>,
    M: 'static + Default + std::hash::Hash + Eq + MessageBuilder + Clone,
{
    let key_clone = key.clone();
    Center::column(
        Expander::horizontal(()),
        Expander::vertical(Label::from(label)),
        Canvas::from(Close::default())
            .class("tab")
            .padding(9.)
            .style("minimal")
            .class("icon")
            .class("flat")
            .class("pill")
            .button(move |this, ctx: &mut UpdateContext<T>, env, p| match p {
                Pointer::Enter | Pointer::Leave => {
                    this.event(ctx, STYLESHEET_SIGNAL, env);
                }
                _ => {
                    if p.left_button_click().is_some() {
                        cell.set(false);
                    	MessageHandler::<_>::post(ctx,ViewOperation::Remove(&key_clone));
                    }
                }
            })
            .flex(Orientation::Vertical),
    )
    .clamp()
    .style("flat")
    .class("compact")
    .class("rounded")
    .class("button")
    .class("")
    .activate(
        key.clone(),
        |this, state, _ctx: &mut UpdateContext<T>, env| {
            if state {
                this.set_class("selected")
            } else {
                this.set_class("")
            }
            env.map(this)
        },
    )
    .button(move |this, ctx, env, p| match p {
        Pointer::Enter | Pointer::Leave => {
            this.event(ctx, STYLESHEET_SIGNAL, env);
        }
        _ => {
            if p.left_button_click().is_some() && !ctx.get(&key).release() {
            	MessageHandler::<_>::post(ctx,ViewOperation::Select(&key));
            }
        }
    })
    .padding_left(2.)
    .padding_right(2.)
}

widget! {
    /// A bar that display tabs representing views.
    AdwTabBar {
        pub fn new<'s, M, T, F>(
            mut f: F,
            entries: impl IntoIterator<Item = (&'s str, M)>,
        ) -> impl Widget<T>
        where
            for<'m> T: 'static + Request<M> + MessageHandler<ViewOperation<'m, M>>,
            F: for<'a> FnMut(&'a mut UpdateContext<T>, &Env) -> Option<(&'a str, M)> + 'static,
            M: 'static + Default + Clone + std::hash::Hash + Eq + MessageBuilder,
            for<'m> M::Message<'m>: Release<bool>,
        {
            let mut tabbar = TabBar::new(move |ctx, env| {
                f(ctx, env)
                    .map(|(label, key)| {
                        InnerTab::new(|cell| tab(cell, label, key))
                    })
            });

            for (label, key) in entries {
                tabbar.add(|cell| {
                    tab(cell, label, key)
                })
            }

            tabbar
        }
    }
}

widget! {
    /// A view switcher.
    ///
    /// Meant to be used in tandem with [`Tab`].
    AdwViews {
        pub fn new<'a, M, T>(entries: impl IntoIterator<Item = (&'a str, M)> + 'a) -> impl Iterator<Item = impl Widget<T>>  + 'a
        where
            T: 'a,
            M: Default + std::hash::Hash + Eq + MessageBuilder + Clone,
            for<'m> M::Message<'m>: Release<bool>,
            T: Request<M>,
            for<'m> T: MessageHandler<ViewOperation<'m, M>>,
        {
            entries
                .into_iter()
                .map(|(label, key)| {
                    let key_clone = key.clone();
                    Label::from(label)
                    .clamp()
                    .anchor(START, CENTER)
                    .style("flat")
                    .class("button")
                    .class("rounded")
                    .class("")
                    .activate(key, |this, state, _ctx: &mut UpdateContext<T>, env| {
                        if state {
                            this.set_class("selected")
                        } else {
                            this.set_class("")
                        }
                        env.map(this)
                    })
                    .button(move |this, ctx, env, p| match p {
                        Pointer::Enter | Pointer::Leave => env.map(this),
                        _ => {
                            if p.left_button_click().is_some() {
                            	MessageHandler::<_>::post(ctx,ViewOperation::Select(&key_clone));
                            }
                        }
                    })
                    .padding(2.)
            })
        }
    }
}

widget! {
    /// A list of widgets.
    AdwList {
        /// Each list element is a button.
        pub fn new<'a, T, F>(entries: impl IntoIterator<Item = &'a str> + 'a, f: F) -> impl Iterator<Item = impl Widget<T> + Themeable>  + 'a
        where
            T: 'a,
            F: FnMut(&str, &mut UpdateContext<T>) + Clone + 'static
        {
            entries
                .into_iter()
                .map(Label::from)
                .map(move |label| {
                    let mut f_clone = f.clone();
                    label
                    .clamp()
                    .style("button")
                    .button(move |this, ctx, env, p| match p {
                        Pointer::Enter | Pointer::Leave => env.map(this),
                        _ => {
                            if p.left_button_click().is_some() {
                                f_clone(this.as_str(), ctx)
                            }
                        }
                    })
                })
        }
        /// Each list element is flat.
        pub fn flat<'a, T, F>(entries: impl IntoIterator<Item = &'a str> + 'a, f: F) -> impl Iterator<Item = impl Widget<T> + Themeable>  + 'a
        where
            T: 'a,
            F: FnMut(&str, &mut UpdateContext<T>) + Clone + 'static
        {
            entries
                .into_iter()
                .map(Label::from)
                .map(move |label| {
                    let mut f_clone = f.clone();
                    label
                    .clamp()
                    .style("button")
                    .class("flat")
                    .button(move |this, ctx, env, p| match p {
                        Pointer::Enter | Pointer::Leave => env.map(this),
                        _ => {
                            if p.left_button_click().is_some() {
                                f_clone(this.as_str(), ctx)
                            }
                        }
                    })
                })
        }
    }
}

widget! {
    /// A list of widgets made for popups.
    AdwPopupList {
        pub fn new<'a, T, F>(entries: impl IntoIterator<Item = &'a str>, f: F) -> impl Widget<T> + Themeable + 'static
        where
            T: 'static,
            F: FnMut(&str, &mut UpdateContext<T>) + Clone + 'static
        {
            entries
                .into_iter()
                .map(Label::from)
                .map(|label| {
                    let mut f_clone = f.clone();
                    Expander::horizontal(
                        label
                    )
                    .alignment(START)
                    .style("flat")
                    .class("button")
                    .class("rounded")
                    .button(move |this, ctx: &mut UpdateContext<T>, env, p| match p {
                        Pointer::Enter | Pointer::Leave => env.map(this),
                        _ => {
                            if p.left_button_click().is_some() {
                                ctx.window().close();
                                f_clone(this.as_str(), ctx)
                            }
                        }
                    })
                })
                .collect::<Row<_, _>>()
                .style("menu")
                .class("frame")
        }
    }
}

widget! {
    /// An horizontal split.
    Hsplit {
        pub fn new<T>() -> impl Widget<T> + Themeable {
            Rectangle::default()
                .class("seperator")
                .with_fixed_height(1.)
        }
    }
}

widget! {
    /// A vertical split.
    Vsplit {
        pub fn new<T>() -> impl Widget<T> + Themeable {
            Rectangle::default()
                .class("seperator")
                .with_fixed_width(1.)
        }
    }
}

widget! {
    /// A window headerbar.
    AdwHeader {
        pub fn new<T>(left: impl Widget<T>, center: impl Widget<T>) -> impl Widget<T> + Themeable + Style {
            Center::column(
                left.clamp().anchor(START,CENTER),
                center.flex(Orientation::Vertical),
                Close::new()
                .with_max_size(28., 28.)
                .button(move |this, ctx: &mut UpdateContext<T>, env, p| match p {
                    Pointer::Leave | Pointer::Enter => {
                        env.map(this.deref_mut());
                    }
                    _ => {
                        if p.left_button_click().is_some() {
                            ctx.window().close();
                        }
                    }
                })
                .flex(Orientation::Vertical)
            )
            .with_fixed_height(35.)
            .style("titlebar")
        }
    }
}

widget! {
    /// A window.
    AdwWindow {
        pub fn new<T: 'static>(header: impl Widget<T> + Themeable + Style, widget: impl Widget<T> + 'static) -> impl Widget<T> + Themeable {
            Window::new(
                header,
                Row!(
                    Rectangle::default()
                        .class("border")
                        .with_fixed_height(1.),
                    widget
                )
            )
        }
    }
}

widget! {
    /// A message dialog menu.
    AdwMessageDialog {
        pub fn new<T>(
            title: &str,
            body: &str,
            destructive_action: &str,
            suggested_action: &str,
            mut destructive_cb: impl FnMut(&mut UpdateContext<T>) + 'static,
            mut suggested_cb: impl FnMut(&mut UpdateContext<T>) + 'static
        ) -> impl Widget<T> + Themeable
        where
            T: 'static,
        {
            Center::row(
                Expander::horizontal(Label::from(title).class("heading")).clamp(),
                Expander::horizontal(Label::from(body)).padding(20.) ,
                Center::column(
                    Label::from(destructive_action)
                        .class("title-3")
                        .clamp()
                        .style("button")
                        .class("rounded")
                        .class("compact")
                        .button(move |this, ctx: &mut UpdateContext<T>, env, p| match p {
                            Pointer::Enter | Pointer::Leave => {
                                env.map(this);
                            }
                            _ => if p.left_button_click().is_some() {
                                ctx.window().close();
                                destructive_cb(ctx);
                            }
                        }),
                    Spacer::horizontal(5.),
                    Label::from(suggested_action)
                        .class("title-3")
                        .clamp()
                        .style("destructive-action")
                        .class("button")
                        .class("rounded")
                        .class("compact")
                        .button(move |this, ctx: &mut UpdateContext<T>, env, p| match p {
                            Pointer::Enter | Pointer::Leave => {
                                env.map(this);
                            }
                            _ => if p.left_button_click().is_some() {
                                ctx.window().close();
                                suggested_cb(ctx);
                            }
                        }),
                )
                .with_fixed_height(40.)
            )
            .style("dialog")
            .class("compact")
            .class("frame")
            .button(|_, ctx, _, p| {
                if let Some(serial) = p.left_button_click() {
                    ctx.window()._move(serial);
                }
            })
        }
    }
}
