use msg_api::message::*;
use msg_api::*;
use snui::scene::Position;
use snui::widgets::{msg::*, *};
use snui::*;
use snui_adwaita::{
    adwaita::*,
    widgets::{
        list::{ColumnItem, RowItem},
        *,
    },
    AdwButtonBuilder,
};
use snui_wayland::*;

#[derive(Clone)]
struct Demo {
    toggle: bool,
    scale: f32,
    view: Views,
    input: String,
    current: Tab,
    tabs: [bool; 3],
}

impl Default for Demo {
    fn default() -> Self {
        Self {
            toggle: false,
            scale: 0.,
            view: Views::Main,
            input: String::new(),
            current: Tab(1),
            tabs: [true; 3],
        }
    }
}

// Define all application messages.

Messages! {
    Views<bool> {
        Main,
        Dialog,
        Slider,
        Tabs,
        Input,
        Popup,
        Button,
    }
}

impl Default for Views {
    fn default() -> Self {
        Views::Main
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Hash, Default, Debug)]
pub struct Tab(usize);

impl MessageBuilder for Tab {
    type Data<'a> = bool;
    type Message<'a> = Msg<'a, Self>;
    fn from<'a>(&self, data: bool) -> Self::Message<'a> {
        Msg::new(data)
    }
}

Messages! {
    Scale<f32>,
    Multipier<bool>,
    Text<&'a str>,
    ScaleStr<String>,
    View<bool>,
    ResetTab<()>
}

// Handle all incomming messages.

MessageHandler! {
    Demo {
        Scale => (this, msg) {
            this.scale = msg.get() * 100.;
        }
        Multipier => (this, msg) {
            this.toggle = msg.get();
        }
        Text => (this, msg) {
            this.input.replace_range(0.., msg.get());
        }
        ResetTab => (this, _) {
            this.tabs = [true;3];
        }
    }
}

impl MessageHandler<ViewOperation<'_, Views>> for Demo {
    fn post(&mut self, message: ViewOperation<'_, Views>) {
        if let ViewOperation::Select(view) = message {
            self.view = *view;
        }
    }
}

impl MessageHandler<ViewOperation<'_, Tab>> for Demo {
    fn post(&mut self, message: ViewOperation<'_, Tab>) {
        match message {
            ViewOperation::Select(view) => self.current = *view,
            ViewOperation::Remove(view) => {
                self.tabs[view.0 - 1] = false;
                for i in 0..self.tabs.len() {
                    if self.tabs[i] {
                        self.current = Tab(i + 1);
                    }
                }
            }
        }
    }
}

// Handle all potential requests.

RequestManager! {
    Demo {
        Scale => (this, req) {
            req.from(this.scale / 100.)
        }
        Multipier => (this, req) {
            req.from(this.toggle)
        }
        Text => (this, req) {
            req.from(this.input.as_str())
        }
        ScaleStr => (this, req) {
            req.from(((this.scale as u32) * this.toggle.then_some(2).unwrap_or(1)).to_string())
        }
        Views => (this, req) {
            req.from(this.view.eq(req))
        }
        Tab => (this, req) {
            req.from(this.current.eq(req))
        }
    }
}

impl Request<HasView<'_, Views>> for Demo {
    fn get<'a>(
        &'a self,
        _desc: &HasView<'_, Views>,
    ) -> <HasView<'_, Views> as MessageBuilder>::Message<'a> {
        true
    }
}

impl Request<CurrentView<Views>> for Demo {
    fn get<'a>(
        &'a self,
        _desc: &CurrentView<Views>,
    ) -> <CurrentView<Views> as MessageBuilder>::Message<'a> {
        self.view
    }
}

impl Request<CurrentView<Tab>> for Demo {
    fn get<'a>(
        &'a self,
        _desc: &CurrentView<Tab>,
    ) -> <CurrentView<Tab> as MessageBuilder>::Message<'a> {
        self.current
    }
}

impl Request<HasView<'_, Tab>> for Demo {
    fn get<'a>(
        &'a self,
        desc: &HasView<'_, Tab>,
    ) -> <HasView<'_, Tab> as MessageBuilder>::Message<'a> {
        self.tabs[desc.get().0 - 1]
    }
}

impl FieldParameter for Scale {}
impl FieldParameter for Multipier {}
impl FieldParameter for Text {}

fn welcome() -> impl Widget<Demo> {
    Label::from("Welcome to snui!").class("title-1")
}

fn data() -> impl Widget<Demo> {
    Row!(
        DynLabel::new(ScaleStr).class("title-3").clamp(),
        Row!(
            AdwSlider::new::<Scale, _>(Scale).padding(10.),
            AdwProgress::new::<Scale, _>(Scale).padding(10.),
        )
        .clamp(),
        Center::column(
            Label::from("Click it!")
                .class("")
                .clamp()
                .anchor(START, CENTER),
            (),
            AdwToggle::new::<Multipier, _>(Multipier).flex(Orientation::Vertical)
        )
        .style("card")
        .class("rounded")
        .class("frame")
        .with_fixed_height(45.)
        .clamp()
        .anchor(CENTER, END)
    )
}

fn dialog<T: 'static>() -> impl Widget<T> {
    AdwButtonBuilder!(T: "Open Dialog", |ctx: &mut UpdateContext<T>, _| {
        ctx.create_popup(|_| {
            Menu::toplevel(
                "Menu",
                AdwMessageDialog::new(
                    "Hello world",
                    "This message is completely useless.",
                    "Cancel",
                    "Nothing",
                    |_| {},
                    |_| {},
                )
                .class("frame")
                .with_min_size(300., 150.),
            )
        });
    }, "accent" | "pill")
    .with_fixed_height(40.)
}

fn tabs() -> impl Widget<Demo> {
    AdwButtonBuilder!(Demo: "Open tab demo", |ctx: &mut UpdateContext<Demo>, _| {
        MessageHandler::<_>::post(ctx, ResetTab.from(())); 
        ctx.create_popup(|_| {
            Menu::toplevel(
                "Tabs",
                AdwWindow::new(
                    AdwHeader::new(
                        (),
                        AdwTabBar::new::<Tab, _, _>(|_, _| None, [("Tab 1", Tab(1)), ("Tab 2", Tab(2)), ("Tab 3", Tab(3))])
                            .with_max_width(0.)
                            .with_rel_width(RelativeUnit::Percent(80.)),
                    ),
                    Tab!(
                        Tab(1) => Label::from("Tab 1"),
                        Tab(2) => Label::from("Tab 2"),
                        Tab(3) => Label::from("Tab 3"),
                    )
                    .with_min_size(450., 200.)
                    .padding(10.),
                ),
            )
        });
    }, "accent" | "pill")
    .with_fixed_height(40.)
}

#[derive(Clone, PartialEq, Default)]
pub struct Hotspot {
    position: Position,
}

impl Widget<Demo> for Hotspot {
    fn draw_scene(&mut self, scene: scene::Scene, _env: &Env) {
        self.position = scene.position();
    }
    fn event(&mut self, ctx: &mut UpdateContext<Demo>, event: Event, _env: &Env) -> Damage {
        if let Event::Pointer(MouseEvent { pointer, position }) = event {
            if let Some(serial) = pointer.left_button_click() {
                ctx.create_popup(|_| {
                    PopupBuilder::default()
                        .anchor(START, START)
                        .offset(self.position + position)
                        .serial(serial)
                        .finish(
                            AdwPopupList::new(
                                ["Item 1", "Item 2", "Item 3"],
                                |name, ctx: &mut UpdateContext<Demo>| {
                                    println!("{}", name);
                                    ctx.window().close()
                                },
                            )
                            .with_min_width(100.),
                        )
                })
            }
        }
        Damage::None
    }
    fn update(&mut self, _ctx: &mut UpdateContext<Demo>, _env: &Env) -> Damage {
        Damage::None
    }
    fn layout(&mut self, _ctx: &mut LayoutContext<Demo>, bc: &BoxConstraints, _env: &Env) -> Size {
        bc.maximum()
    }
}

fn popup() -> impl Widget<Demo> {
    Row!(
        Label::from("Click below!")
            .class("title-3")
            .padding(10.)
            .flex(Orientation::Horizontal),
        Hotspot::default()
            .style("view")
            .class("frame")
            .class("rounded")
    )
}

fn input() -> impl Widget<Demo> {
    AdwButtonBuilder!(Demo: "Open input demo", |ctx: &mut UpdateContext<Demo>, _| {
        ctx.create_popup(|_| {
        	Menu::Toplevel {
        		title: String::from("Input"),
        		size: BoxConstraints::infinite().with_min(300., 150.),
        		widget: Box::new(
                    AdwWindow::new(
                        AdwHeader::new((), Label::from("Input")),
                        Row!(
                            DynLabel::new(Text).clamp(),
                            AdwInput::new::<Text, _>(Text).padding(10.).with_min_width(100.)
                        ),
                    )
        		)
    		} 
        })
    }, "accent" | "pill")
    .with_fixed_height(40.)
}

fn demo() -> impl Widget<Demo> {
    AdwWindow::new(
        AdwHeader::new((), Label::from("Demo").class("title-3")),
        Center::column(
            Scrollbox::new(
                AdwViews::new::<Views, _>([
                    ("Welcome", Views::Main),
                    ("Slider", Views::Slider),
                    ("Tabs", Views::Tabs),
                    ("Dialog", Views::Dialog),
                    ("Input", Views::Input),
                    ("Button", Views::Button),
                    ("Popup", Views::Popup),
                ])
                .collect::<Row<_, _>>()
                .with_fixed_height(0.),
            )
            .padding(5.)
            .with_max_width(200.)
            .anchor(CENTER, START),
            Vsplit::new(),
            Tab!(
                Views::Slider => data(),
                Views::Main => welcome(),
                Views::Dialog => dialog(),
                Views::Tabs => tabs(),
                Views::Input => input(),
                Views::Popup => popup(),
                Views::Button => buttons(),
            )
            .clamp()
            .padding(10.),
        ),
    )
}

fn buttons() -> impl Widget<Demo> {
    Row!(
        Column!(
            AdwButton::accent("Accent", |_, _| {}).padding(5.),
            AdwButton::new("Regular", |_, _| {}).padding(5.),
            AdwButton::destructive("Destructive", |_, _| {}).padding(5.),
        )
        .with_fixed_height(60.),
        Column!(
            AdwList::new(["Left", "Center", "Right"], |_, _| {})
                .map(|w| ColumnItem::new(w.class("rounded")))
                .collect::<Column<_, _>>()
                .class("linked")
                .with_fixed_height(60.)
                .clamp(),
            AdwList::new(["Top", "Middle", "Bottom"], |_, _| {})
                .map(|w| RowItem::new(w.class("rounded")))
                .collect::<Row<_, _>>()
                .class("linked")
                .with_fixed_width(100.)
                .clamp()
        )
        .clamp(),
        Column!(
            AdwButtonBuilder!(Demo: "Success", |_, _| {}, "success" | "pill").padding(5.),
            AdwButtonBuilder!(Demo: "Regular", |_, _| {}, "frame" | "pill").padding(5.),
            AdwButtonBuilder!(Demo: "Warning", |_, _| {}, "warning" | "pill" | "frame").padding(5.),
        )
        .with_fixed_height(60.),
    )
}

fn main() {
    WaylandClient::init(Demo::default(), |client, qh| {
        client.set_app_id("demo");
        client.set_environment(AdwaitaTheme::default());
        let mut view = client.create_window(demo(), qh);
        view.set_min_size(450, 350);
        view.set_title("Demo".to_string())
    })
}
