//! Implementation of the Adwaita design language for snui.

use snui::environment::*;
use snui::scene::Texture;
use snui::tiny_skia::Color;
use snui::{utils::List, *};

use crate::load_theme;

#[allow(non_snake_case)]
pub mod palette {
    //! Adwaita Color Palette
    pub const BLUE1: u32 = 0xff99c1f1;
    pub const BLUE2: u32 = 0xff62a0ea;
    pub const BLUE3: u32 = 0xff3584e4;
    pub const BLUE4: u32 = 0xff1c71d8;
    pub const BLUE5: u32 = 0xff99c1f1;

    pub const GREEN1: u32 = 0xff8ff0a4;
    pub const GREEN2: u32 = 0xff57e389;
    pub const GREEN3: u32 = 0xff33d17a;
    pub const GREEN4: u32 = 0xff2ec27e;
    pub const GREEN5: u32 = 0xff26a269;

    pub const YELLOW1: u32 = 0xfff9f06b;
    pub const YELLOW2: u32 = 0xfff8e45c;
    pub const YELLOW3: u32 = 0xfff6d32d;
    pub const YELLOW4: u32 = 0xfff5c211;
    pub const YELLOW5: u32 = 0xffe5a50a;

    pub const ORANGE1: u32 = 0xffffbe6f;
    pub const ORANGE2: u32 = 0xffffa348;
    pub const ORANGE3: u32 = 0xffff7800;
    pub const ORANGE4: u32 = 0xffe66100;
    pub const ORANGE5: u32 = 0xffc64600;

    pub const RED1: u32 = 0xfff66151;
    pub const RED2: u32 = 0xffed333b;
    pub const RED3: u32 = 0xffe01b24;
    pub const RED4: u32 = 0xffc01c28;
    pub const RED5: u32 = 0xffa51d2d;

    pub const PURPLE1: u32 = 0xffdc8add;
    pub const PURPLE2: u32 = 0xffc061cb;
    pub const PURPLE3: u32 = 0xff9141ac;
    pub const PURPLE4: u32 = 0xff813d9c;
    pub const PURPLE5: u32 = 0xff613583;

    pub const BROWN1: u32 = 0xffcdab8f;
    pub const BROWN2: u32 = 0xffb5835a;
    pub const BROWN3: u32 = 0xff986a44;
    pub const BROWN4: u32 = 0xff865e3c;
    pub const BROWN5: u32 = 0xff63452c;

    pub const LIGHT1: u32 = 0xffffffff;
    pub const LIGHT2: u32 = 0xfff6f5f4;
    pub const LIGHT3: u32 = 0xffdeddda;
    pub const LIGHT4: u32 = 0xffc0bfbc;
    pub const LIGHT5: u32 = 0xff9a9996;

    pub const DARK1: u32 = 0xff77767b;
    pub const DARK2: u32 = 0xff5e5c64;
    pub const DARK3: u32 = 0xff3d3846;
    pub const DARK4: u32 = 0xff241f31;
    pub const DARK5: u32 = 0xff000000;
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ThemePreference {
    Light,
    Dark,
}

fn opacity(color: u32, opacity: f32) -> Color {
    let mut color = to_color(color);
    color.apply_opacity(opacity);
    color
}

fn lighthen(color: u32, opacity: f32) -> Color {
    let mut color = to_color(color);
    let mut white = to_color(u32::MAX);
    white.apply_opacity(opacity);
    let white = white.premultiply();
    color.set_red(color.red() + white.red());
    color.set_green(color.green() + white.green());
    color.set_blue(color.blue() + white.blue());
    color.set_alpha(color.alpha() + white.alpha());
    color
}

fn darken(color: u32, opacity: f32) -> Color {
    let mut color = to_color(color);
    let mut white = to_color(u32::MAX);
    white.apply_opacity(opacity);
    let white = white.premultiply();
    color.set_red(color.red() - white.red());
    color.set_green(color.green() - white.green());
    color.set_blue(color.blue() - white.blue());
    color.set_alpha(color.alpha() + white.alpha());
    color
}

// fn last_class<'a>(list: &'a List<Id>) -> Option<&'a str> {
//     list.iter().filter_map(|i| i.class()).next()
// }

fn last_classes<'a>(list: &'a List<Id>) -> impl Iterator<Item = &'a str> {
    let item = list.item();
    list.iter()
        .map_while(move |id| {
            let l_item = id.item();
            // (l_item == item || l_item == "button").then_some(id)
            l_item.eq(item).then_some(id)
        })
        .filter_map(|id| id.class())
}

fn last_ids<'a>(list: &'a List<Id<'a>>) -> impl Iterator<Item = &'a Id<'a>> {
    let item = list.item();
    list.iter().map_while(move |id| {
        let l_item = id.item();
        l_item.eq(item).then_some(id)
    })
}

fn is_hovered(list: &List<Id>) -> bool {
    list.iter()
        .find(|id| id.item() == "button")
        .map(|id| id.class() == Some("hover"))
        .unwrap_or_default()
}

fn has_class(list: &List<Id>, classes: &[&str]) -> bool {
    list.iter()
        .filter_map(|id| id.class())
        .any(|class| classes.contains(&class))
}

fn has_last_class(list: &List<Id>, classes: &[&str]) -> bool {
    last_classes(&list).any(|class| classes.contains(&class))
}

fn get_radius(list: &List<Id>) -> Option<Value<'static>> {
    last_classes(&list)
        .filter_map(|class| match class {
            "rect" => Some(Value::from(0.)),
            "menu" | "dialog" => Some(Value::from(10.)),
            "pill" => Some(Value::Unit(RelativeUnit::Percent(50.))),
            "button" | "tab" => (!has_last_class(
                &list,
                &[
                    "pill", "rounded", "circular", "top", "left", "right", "bottom",
                ],
            ))
            .then(|| Value::from(7.)),
            "rounded" => Some(Value::from(7.)),
            _ => None,
        })
        .next()
}

#[allow(non_snake_case)]
#[derive(Debug, Clone, Copy, PartialEq)]
/// Colorscheme of an Adwaita application.
pub struct AdwaitaPalette {
    // Accent Colors
    pub ACCENT_COLOR: u32,
    pub ACCENT_BG_COLOR: u32,
    pub ACCENT_FG_COLOR: u32,

    // Destructive Colors
    pub DESTRUCTIVE_COLOR: u32,
    pub DESTRUCTIVE_BG_COLOR: u32,
    pub DESTRUCTIVE_FG_COLOR: u32,

    // Success Colors
    pub SUCCESS_COLOR: u32,
    pub SUCCESS_BG_COLOR: u32,
    pub SUCCESS_FG_COLOR: u32,

    // Warning Colors
    pub WARNING_COLOR: u32,
    pub WARNING_BG_COLOR: u32,
    pub WARNING_FG_COLOR: u32,

    // Error Colors
    pub ERROR_COLOR: u32,
    pub ERROR_BG_COLOR: u32,
    pub ERROR_FG_COLOR: u32,

    // Window Colors
    pub WINDOW_BG_COLOR: u32,
    pub WINDOW_FG_COLOR: u32,

    // View Colors
    pub VIEW_BG_COLOR: u32,
    pub VIEW_FG_COLOR: u32,

    // Headerbar Colors
    pub HEADERBAR_BG_COLOR: u32,
    pub HEADERBAR_FG_COLOR: u32,
    pub HEADERBAR_BORDER_COLOR: u32,
    pub HEADERBAR_BACKDROP_COLOR: u32,
    pub HEADERBAR_SHADE_COLOR: u32,

    // Card Colors
    pub CARD_BG_COLOR: u32,
    pub CARD_FG_COLOR: u32,
    pub CARD_SHADE_COLOR: u32,

    // Dialog Colors
    pub DIALOG_BG_COLOR: u32,
    pub DIALOG_FG_COLOR: u32,

    // Popover Colors
    pub POPOVER_BG_COLOR: u32,
    pub POPOVER_FG_COLOR: u32,
}

impl AdwaitaPalette {
    pub const fn light() -> Self {
        Self {
            // Accent Colors
            ACCENT_COLOR: 0xff1c71d8,
            ACCENT_BG_COLOR: 0xff3584e4,
            ACCENT_FG_COLOR: 0xffffffff,

            // Destructive Colors
            DESTRUCTIVE_COLOR: 0xffc01c28,
            DESTRUCTIVE_BG_COLOR: 0xffe01b24,
            DESTRUCTIVE_FG_COLOR: 0xffffffff,

            // Success Colors
            SUCCESS_COLOR: 0xff26a269,
            SUCCESS_BG_COLOR: 0xff2ec27e,
            SUCCESS_FG_COLOR: 0xffffffff,

            // Warning Colors
            WARNING_COLOR: 0xffae7b03,
            WARNING_BG_COLOR: 0xffe5a50a,
            WARNING_FG_COLOR: 0x14000000,

            // Error Colors
            ERROR_COLOR: 0xffc01c28,
            ERROR_BG_COLOR: 0xffe01b24,
            ERROR_FG_COLOR: 0xffffffff,

            // Window Colors
            WINDOW_BG_COLOR: 0xfffafafa,
            WINDOW_FG_COLOR: 0xcc000000,

            // View Colors
            VIEW_BG_COLOR: 0xffffffff,
            VIEW_FG_COLOR: 0xcc000000,

            // Headerbar Colors
            HEADERBAR_BG_COLOR: 0xffebebeb,
            HEADERBAR_FG_COLOR: 0xcc000000,
            HEADERBAR_BORDER_COLOR: 0xcc000000,
            HEADERBAR_BACKDROP_COLOR: 0xcc242424,
            HEADERBAR_SHADE_COLOR: 0x12fafafa,

            // Card Colors
            CARD_BG_COLOR: 0xffffffff,
            CARD_FG_COLOR: 0xcc242424,
            CARD_SHADE_COLOR: 0x12000000,

            // Dialog Colors
            DIALOG_BG_COLOR: 0xfffafafa,
            DIALOG_FG_COLOR: 0xcc000000,

            // Popover Colors
            POPOVER_BG_COLOR: 0xfffafafa,
            POPOVER_FG_COLOR: 0xcc242424,
        }
    }
    pub const fn dark() -> Self {
        Self {
            // Accent Colors
            ACCENT_COLOR: 0xff78aeed,
            ACCENT_BG_COLOR: 0xff3584e4,
            ACCENT_FG_COLOR: 0xffffffff,

            // Destructive Colors
            DESTRUCTIVE_COLOR: 0xffff7b63,
            DESTRUCTIVE_BG_COLOR: 0xffc01c28,
            DESTRUCTIVE_FG_COLOR: 0xffffffff,

            // Success Colors
            SUCCESS_COLOR: 0xff8ff0a4,
            SUCCESS_BG_COLOR: 0xff26a269,
            SUCCESS_FG_COLOR: 0xffffffff,

            // Warning Colors
            WARNING_COLOR: 0xfff8e45c,
            WARNING_BG_COLOR: 0xffcd9309,
            WARNING_FG_COLOR: 0xffffffff,

            // Error Colors
            ERROR_COLOR: 0xffff7b63,
            ERROR_BG_COLOR: 0xffc01c28,
            ERROR_FG_COLOR: 0xffffffff,

            // Window Colors
            WINDOW_BG_COLOR: 0xff242424,
            WINDOW_FG_COLOR: 0xffffffff,

            // View Colors
            VIEW_BG_COLOR: 0xff1e1e1e,
            VIEW_FG_COLOR: 0xffffffff,

            // Headerbar Colors
            HEADERBAR_BG_COLOR: 0xff303030,
            HEADERBAR_FG_COLOR: 0xffffffff,
            HEADERBAR_BORDER_COLOR: 0xffffffff,
            HEADERBAR_BACKDROP_COLOR: 0xff242424,
            HEADERBAR_SHADE_COLOR: 0x5c000000,

            // Card Colors
            CARD_BG_COLOR: 0x14ffffff,
            CARD_FG_COLOR: 0xffffffff,
            CARD_SHADE_COLOR: 0x5c000000,

            // Dialog Colors
            DIALOG_BG_COLOR: 0xff383838,
            DIALOG_FG_COLOR: 0xffffffff,

            // Popover Colors
            POPOVER_BG_COLOR: 0xff383838,
            POPOVER_FG_COLOR: 0xffffffff,
        }
    }
}

/// An [`Environment`] implementing GNOME's Human Interface Guidelines.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct AdwaitaTheme {
    preference: ThemePreference,
    palette: AdwaitaPalette,
    high_contrast: bool,
}

impl Default for AdwaitaTheme {
    fn default() -> Self {
        match std::env::var("SNUI_THEME") {
            Ok(var) => match var.as_str() {
                "light" => AdwaitaTheme::light(),
                "dark" => AdwaitaTheme::dark(),
                "system" => load_theme(None).unwrap_or(AdwaitaTheme::light()),
                _ => load_theme(Some(var.as_str())).unwrap_or(AdwaitaTheme::light()),
            },
            Err(_) => AdwaitaTheme::light(),
        }
    }
}

impl AdwaitaTheme {
    pub fn new(preference: ThemePreference, palette: AdwaitaPalette) -> Self {
        Self {
            preference,
            palette,
            high_contrast: false,
        }
    }
    pub const fn dark() -> Self {
        Self {
            preference: ThemePreference::Dark,
            palette: AdwaitaPalette::dark(),
            high_contrast: false,
        }
    }
    pub const fn light() -> Self {
        Self {
            preference: ThemePreference::Light,
            palette: AdwaitaPalette::light(),
            high_contrast: false,
        }
    }
    pub const fn high_contrast(mut self) -> Self {
        self.high_contrast = true;
        self
    }
}

impl Environment for AdwaitaTheme {
    fn get(&self, list: List<Id>, property: &str) -> Option<Value> {
        let alpha = self.high_contrast.then_some(0.5).unwrap_or(0.15);
        match property {
            "color" | "foreground" => list
                .iter()
                .filter_map(|id| match id.class().unwrap_or_default() {
                    "dim" | "dim-label" => Some(opacity(self.palette.WINDOW_FG_COLOR, 0.5).into()),
                    "accent" | "suggested-action" => Some(
                        match list.item() {
                            "progress" => self.palette.ACCENT_BG_COLOR,
                            "label" => self.palette.ACCENT_FG_COLOR,
                            _ => self.palette.ACCENT_COLOR,
                        }
                        .into(),
                    ),
                    "destructive-action" => Some(
                        match list.item() {
                            "progress" => self.palette.DESTRUCTIVE_BG_COLOR,
                            "label" => self.palette.DESTRUCTIVE_FG_COLOR,
                            _ => self.palette.DESTRUCTIVE_COLOR,
                        }
                        .into(),
                    ),
                    "success" => Some(
                        match list.item() {
                            "progress" => self.palette.SUCCESS_BG_COLOR,
                            "label" => self.palette.SUCCESS_FG_COLOR,
                            _ => self.palette.SUCCESS_COLOR,
                        }
                        .into(),
                    ),
                    "warning" => Some(
                        match list.item() {
                            "progress" => self.palette.WARNING_BG_COLOR,
                            "label" => self.palette.WARNING_FG_COLOR,
                            _ => self.palette.WARNING_COLOR,
                        }
                        .into(),
                    ),
                    "error" => Some(
                        match list.item() {
                            "progress" => self.palette.ERROR_BG_COLOR,
                            "label" => self.palette.ERROR_FG_COLOR,
                            _ => self.palette.ERROR_COLOR,
                        }
                        .into(),
                    ),
                    "tab" => Some(if has_class(&list, &["hover"]) {
                        self.palette.VIEW_FG_COLOR.into()
                    } else {
                        Texture::Transparent.into()
                    }),
                    "linked" => property
                        .ne("foreground")
                        .then(|| match self.preference {
                            ThemePreference::Light => {
                                Color::from_rgba(0., 0., 0., alpha).map(|c| c.into())
                            }
                            ThemePreference::Dark => {
                                Color::from_rgba(1., 1., 1., alpha).map(|c| c.into())
                            }
                        })
                        .flatten(),
                    "border" => Color::from_rgba(0., 0., 0., alpha).map(|c| c.into()),
                    "seperator" => match self.preference {
                        ThemePreference::Light => Color::from_rgba(0., 0., 0., alpha),
                        ThemePreference::Dark => Color::from_rgba(1., 1., 1., alpha),
                    }
                    .map(|c| c.into()),
                    "icon" => Some(self.palette.VIEW_FG_COLOR.into()),
                    "view" => Some(self.palette.VIEW_FG_COLOR.into()),
                    "dialog" => Some(self.palette.DIALOG_FG_COLOR.into()),
                    "popover" | "menu" => Some(self.palette.POPOVER_FG_COLOR.into()),
                    "window" => Some(self.palette.WINDOW_FG_COLOR.into()),
                    _ => match list.item() {
                        "icon" => Some(self.palette.WINDOW_FG_COLOR.into()),
                        "toggle" => Some(match self.preference {
                            ThemePreference::Dark => self.palette.CARD_FG_COLOR.into(),
                            ThemePreference::Light => self.palette.CARD_BG_COLOR.into(),
                        }),
                        "handle" => Some(match self.preference {
                            ThemePreference::Dark => self.palette.CARD_FG_COLOR.into(),
                            ThemePreference::Light => self.palette.CARD_BG_COLOR.into(),
                        }),
                        // "progress" | "slider" => Some(match self.preference {
                        //     ThemePreference::Dark => self.palette.CARD_BG_COLOR.into(),
                        //     ThemePreference::Light => {
                        //         let mut color = lighthen(self.palette.CARD_FG_COLOR, 0.75);
                        //         color.set_alpha(1.);
                        //         color.into()
                        //     }
                        // }),
                        _ => None,
                    },
                })
                .next()
                .or_else(|| Some(self.palette.WINDOW_FG_COLOR.into())),
            "font-style" => list.class().map(|class| {
                if class.starts_with("title") || matches!(class, "heading") {
                    "Bold"
                } else {
                    ""
                }
                .into()
            }),
            "font-family" => list.class().map(|class| {
                if class.starts_with("title") || class == "heading" {
                    "Cantarell"
                } else if class == "monospace" {
                    "Monospace"
                } else {
                    ""
                }
                .into()
            }),
            "font-size" => list.class().map(|class| {
                match class {
                    "title-1" | "heading" => 24.,
                    "title-2" => 20.,
                    "title-3" => 16.,
                    "title-4" | "monospace" => 14.,
                    "large" => 40.,
                    _ => 14.,
                }
                .into()
            }),
            "background" => last_ids(&list)
                .filter(|id| !matches!(id.item(), "boxed-list" | "slider"))
                .filter_map(|id| id.class())
                .filter_map(|class| match class {
                    "accent" | "suggested-action" => Some(self.palette.ACCENT_BG_COLOR),
                    "destructive-action" => Some(self.palette.DESTRUCTIVE_BG_COLOR),
                    "success" => Some(self.palette.SUCCESS_BG_COLOR),
                    "warning" => Some(self.palette.WARNING_BG_COLOR),
                    "error" => Some(self.palette.ERROR_BG_COLOR),
                    "toolbar" | "headerbar" | "titlebar" => Some(self.palette.HEADERBAR_BG_COLOR),
                    "view" => Some(self.palette.VIEW_BG_COLOR),
                    "menu" | "popup" => Some(self.palette.POPOVER_BG_COLOR),
                    "dialog" => Some(self.palette.DIALOG_BG_COLOR),
                    "background" => Some(self.palette.WINDOW_BG_COLOR),
                    "border" | "linked" => {
                        Color::from_rgba(0., 0., 0., alpha).map(|c| c.to_color_u8().get())
                    }
                    "boxed-list" | "card" => Some(self.palette.CARD_BG_COLOR),
                    "button" => Some(
                        match self.preference {
                            ThemePreference::Light => Color::from_rgba(0., 0., 0., 0.08)?,
                            ThemePreference::Dark => Color::from_rgba(1., 1., 1., 0.08)?,
                        }
                        .to_color_u8()
                        .get(),
                    ),
                    "seperator" => Some(
                        opacity(self.palette.HEADERBAR_BORDER_COLOR, alpha)
                            .to_color_u8()
                            .get(),
                    ),
                    "icon" => Some(
                        match self.preference {
                            ThemePreference::Dark => Color::from_rgba(1., 1., 1., 0.1)?,
                            ThemePreference::Light => Color::from_rgba(0., 0., 0., 0.1)?,
                        }
                        .to_color_u8()
                        .get(),
                    ),
                    "toggle" => Some(
                        match self.preference {
                            ThemePreference::Dark => lighthen(self.palette.CARD_BG_COLOR, 0.1),
                            ThemePreference::Light => Color::from_rgba(0., 0., 0., 0.1)?,
                        }
                        .to_color_u8()
                        .get(),
                    ),
                    "progress" | "slider" => Some(match self.preference {
                        ThemePreference::Dark => self.palette.CARD_BG_COLOR,
                        ThemePreference::Light => Color::from_rgba(0., 0., 0., 0.1)
                            .unwrap()
                            .to_color_u8()
                            .get(),
                    }),
                    "osd" => Some(0xcc000000),
                    _ => None,
                })
                .map(|mut color| {
                    let rule = last_classes(&list)
                        .find(|class| matches!(*class, "raised" | "selected"))
                        .unwrap_or_default();
                    if last_classes(&list).any(|class| class == "opaque") {
                        color = opacity(color, 1.).to_color_u8().get();
                    }
                    if rule == "selected" {
                        match self.preference {
                            ThemePreference::Dark => lighthen(color, 0.05).into(),
                            ThemePreference::Light => darken(color, 0.05).into(),
                        }
                    } else if rule == "opaque" {
                        opacity(color, 1.).into()
                    } else if is_hovered(&list) || rule == "raised" {
                        if last_classes(&list).any(|class| class == "flat") {
                            color.into()
                        } else {
                            match self.preference {
                                ThemePreference::Dark => lighthen(color, 0.05).into(),
                                ThemePreference::Light => darken(color, 0.05).into(),
                            }
                        }
                    } else if last_classes(&list).any(|class| class == "flat") {
                        opacity(color, 0.).into()
                    } else {
                        color.into()
                    }
                })
                .next()
                .or_else(|| {
                    Some(
                        match list.item() {
                            "handle" => match self.preference {
                                ThemePreference::Dark => self.palette.CARD_FG_COLOR,
                                ThemePreference::Light => self.palette.CARD_BG_COLOR,
                            },
                            "header" => self.palette.HEADERBAR_BG_COLOR,
                            "window" => self.palette.WINDOW_BG_COLOR,
                            "progress" | "slider" => match self.preference {
                                ThemePreference::Dark => self.palette.CARD_BG_COLOR,
                                ThemePreference::Light => {
                                    let mut color = lighthen(self.palette.CARD_FG_COLOR, 0.75);
                                    color.set_alpha(1.);
                                    color.to_color_u8().get()
                                }
                            },
                            _ => {
                                dbg!(list);
                                todo!()
                            }
                        }
                        .into(),
                    )
                }),
            "highlight" => matches!(list.item(), "slider" | "progress")
                .then(|| match list.class().unwrap_or_default() {
                    "accent" | "suggested-action" => Some(self.palette.ACCENT_BG_COLOR.into()),
                    "destructive-action" => Some(self.palette.DESTRUCTIVE_BG_COLOR.into()),
                    "success" => Some(self.palette.SUCCESS_BG_COLOR.into()),
                    "warning" => Some(self.palette.WARNING_BG_COLOR.into()),
                    "error" => Some(self.palette.ERROR_BG_COLOR.into()),
                    "background" => Some(self.palette.WINDOW_BG_COLOR.into()),
                    _ => None,
                })
                .flatten(),
            "radius" | "circular" => match list.item() {
                "slider" | "progress" => last_classes(&list)
                    .filter_map(|class| match class {
                        "osd" => Some(Value::from(0.)),
                        "pill" => Some(Value::Unit(RelativeUnit::Percent(50.))),
                        _ => None,
                    })
                    .next(),
                "window" => Some(Value::from(10.)),
                "cursor" => None,
                _ => (!has_class(&list, &["linked"]))
                    .then(|| get_radius(&list))
                    .flatten(),
            },
            "top-left-radius" => list
                .iter()
                .filter_map(|id| id.class())
                .filter_map(|class| match class {
                    "top" | "left" => has_class(&list, &["linked"])
                        .then(|| get_radius(&list))
                        .flatten(),
                    _ => None,
                })
                .next(),
            "top-right-radius" => list
                .iter()
                .filter_map(|id| id.class())
                .filter_map(|class| match class {
                    "top" | "right" => has_class(&list, &["linked"])
                        .then(|| get_radius(&list))
                        .flatten(),
                    _ => None,
                })
                .next(),
            "bottom-left-radius" => list
                .iter()
                .filter_map(|id| id.class())
                .filter_map(|class| match class {
                    "bottom" | "left" => has_class(&list, &["linked"])
                        .then(|| get_radius(&list))
                        .flatten(),
                    _ => None,
                })
                .next(),
            "bottom-right-radius" => list
                .iter()
                .filter_map(|id| id.class())
                .filter_map(|class| match class {
                    "bottom" | "right" => has_class(&list, &["linked"])
                        .then(|| get_radius(&list))
                        .flatten(),
                    _ => None,
                })
                .next(),
            "ratio" => matches!(list.item(), "handle").then_some(Value::from(4.)),
            "padding" => last_classes(&list)
                .filter_map(|class| match class {
                    "toggle" => Some(Value::from(0.)),
                    "progress" | "minimal" => Some(Value::from(0.)),
                    "button" | "card" | "icon" | "view" => Some(Value::from(10.)),
                    "menu" | "titlebar" | "headerbar" | "toolbar" | "compact" | "dialog" => {
                        Some(Value::from(5.))
                    }
                    _ => None,
                })
                .next(),
            "border-texture" => list
                .item()
                .eq("window")
                .then(|| match self.preference {
                    ThemePreference::Dark => lighthen(self.palette.WINDOW_BG_COLOR, alpha).into(),
                    ThemePreference::Light => darken(self.palette.WINDOW_BG_COLOR, alpha).into(),
                })
                .or_else(|| {
                    last_classes(&list)
                        .filter_map(|class| match class {
                            "shade" => has_last_class(&list, &["card", "button"])
                                .then(|| self.palette.CARD_SHADE_COLOR.into()),
                            "frame" => {
                                let current_color = list
                                    .iter()
                                    .filter_map(|id| id.class())
                                    .filter_map(|class| match class {
                                        "accent" | "suggested-action" => {
                                            Some(self.palette.ACCENT_COLOR)
                                        }
                                        "destructive-action" => {
                                            Some(self.palette.DESTRUCTIVE_COLOR)
                                        }
                                        "success" => Some(self.palette.SUCCESS_COLOR),
                                        "warning" => Some(self.palette.WARNING_COLOR),
                                        "error" => Some(self.palette.ERROR_FG_COLOR),
                                        "view" => Some(self.palette.VIEW_FG_COLOR),
                                        "window" | "background" => {
                                            Some(self.palette.WINDOW_FG_COLOR)
                                        }
                                        "card" => Some(self.palette.CARD_FG_COLOR),
                                        "dialog" => Some(self.palette.DIALOG_FG_COLOR),
                                        "menu" | "popup" => Some(self.palette.POPOVER_FG_COLOR),
                                        "titlebar" | "headerbar" | "header" => {
                                            Some(self.palette.HEADERBAR_FG_COLOR)
                                        }
                                        _ => None,
                                    })
                                    .next()
                                    .unwrap_or(self.palette.WINDOW_FG_COLOR);
                                Some(opacity(current_color, alpha).into())
                            }
                            _ => None,
                        })
                        .next()
                }),
            "border-width" => list
                .item()
                .eq("window")
                .then_some(Value::from(1.))
                .or_else(|| {
                    last_classes(&list)
                        .filter_map(|class| match class {
                            "frame" | "shade" => Some(Value::from(1.)),
                            _ => None,
                        })
                        .next()
                }),
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! list {
        ( ) => {
            List::new(Id::new("root", None))
                .push(Id::new("item", Some("a")))
                .push(Id::new("button", Some("hover")))
                .push(Id::new("item", Some("b")))
                .push(Id::new("button", None))
                .push(Id::new("box", Some("c")))
                .push(Id::new("box", Some("a")))
                .push(Id::new("box", None))
        };
    }

    macro_rules! list_2 {
        ( ) => {
            List::new(Id::new("root", None))
                .push(Id::new("item", Some("a")))
                .push(Id::new("button", Some("hover")))
                .push(Id::new("style", Some("pill")))
                .push(Id::new("button", None))
                .push(Id::new("style", Some("c")))
                .push(Id::new("style", Some("a")))
                .push(Id::new("style", Some("button")))
        };
    }

    macro_rules! list_3 {
        ( ) => {
            List::new(Id::new("root", None))
                .push(Id::new("item", Some("a")))
                .push(Id::new("button", Some("hover")))
                .push(Id::new("style", Some("pill")))
                .push(Id::new("style", Some("c")))
                .push(Id::new("style", Some("a")))
                .push(Id::new("style", Some("button")))
        };
    }

    #[test]
    // fn last_class_test() {
    //     assert_eq!(last_class(&list!()), Some("a"))
    // }
    #[test]
    fn is_hovered_test() {
        assert_eq!(is_hovered(&list!()), false);
        assert_eq!(is_hovered(&list_3!()), true)
    }

    #[test]
    fn has_last_class_test() {
        assert_eq!(has_last_class(&list!(), &["b"]), false);
        assert_eq!(has_last_class(&list!(), &["a", "b"]), true);
        assert_eq!(has_last_class(&list!(), &["a", "c"]), true);
    }

    #[test]
    fn last_classes_test() {
        assert_eq!(last_classes(&list!()).count(), 2);
    }

    #[test]
    fn last_ids_test() {
        assert_eq!(last_ids(&list!()).count(), 3);
    }

    #[test]
    fn get_radius_test() {
        assert_eq!(
            get_radius(&list_3!()),
            Some(Value::Unit(RelativeUnit::Percent(50.)))
        );
        assert_eq!(
            get_radius(&list_2!()),
            Some(Value::Unit(RelativeUnit::Unit(7.)))
        );
        assert_eq!(get_radius(&list!()), None);
    }
}
