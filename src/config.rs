use super::adwaita::*;
use ini::ini;
use std::env::var;
use std::{collections::HashMap, path::PathBuf};

fn get_key<'a>(
    map: &'a HashMap<String, HashMap<String, Option<String>>>,
    group: &str,
    key: &str,
) -> Option<&'a str> {
    map.get(group)
        .and_then(|group| group.get(key).and_then(|key| key.as_ref()))
        .map(|key| key.as_str())
}

fn get_preference<'a>(
    map: &'a HashMap<String, HashMap<String, Option<String>>>,
    group: &str,
    key: &str,
) -> ThemePreference {
    if let Some(value) = get_key(map, group, key) {
        match value {
            "light" | "default" => ThemePreference::Light,
            "dark" => ThemePreference::Dark,
            _ => panic!("invalid token: {}", value),
        }
    } else {
        ThemePreference::Light
    }
}

fn get_color<'a>(
    map: &'a HashMap<String, HashMap<String, Option<String>>>,
    group: &str,
    key: &str,
) -> u32 {
    if let Some(value) = get_key(map, group, key) {
        match u32::from_str_radix(value, 16) {
            Ok(color) => color,
            Err(err) => {
                panic!("err: {}.{} - {}", group, key, err)
            }
        }
    } else {
        panic!("missing: {}.{}", group, key)
    }
}

/// Load a theme from a file.
pub fn load_theme(path: Option<&str>) -> Option<AdwaitaTheme> {
    let map = if let Some(path) = path {
        if !PathBuf::from(path).as_path().is_file() {
            return None;
        }
        ini!(path)
    } else {
        let mut path = var("XDG_CONFIG_HOME").unwrap_or({
            let mut home = var("HOME").unwrap_or(String::from("~/"));
            home.push_str("/.config");
            home
        });
        path.push_str("/snui/style.ini");
        if !PathBuf::from(&path).as_path().is_file() {
            return None;
        }
        ini!(path.as_str())
    };
    Some(AdwaitaTheme::new(
        get_preference(&map, "style", "preference"),
        AdwaitaPalette {
            ACCENT_BG_COLOR: get_color(&map, "accent", "background"),
            ACCENT_COLOR: get_color(&map, "accent", "color"),
            ACCENT_FG_COLOR: get_color(&map, "accent", "foreground"),

            SUCCESS_BG_COLOR: get_color(&map, "success", "background"),
            SUCCESS_COLOR: get_color(&map, "success", "color"),
            SUCCESS_FG_COLOR: get_color(&map, "success", "foreground"),

            DESTRUCTIVE_BG_COLOR: get_color(&map, "destructive", "background"),
            DESTRUCTIVE_COLOR: get_color(&map, "destructive", "color"),
            DESTRUCTIVE_FG_COLOR: get_color(&map, "destructive", "foreground"),

            ERROR_BG_COLOR: get_color(&map, "error", "background"),
            ERROR_COLOR: get_color(&map, "error", "color"),
            ERROR_FG_COLOR: get_color(&map, "warning", "foreground"),

            WARNING_BG_COLOR: get_color(&map, "warning", "background"),
            WARNING_COLOR: get_color(&map, "warning", "color"),
            WARNING_FG_COLOR: get_color(&map, "warning", "foreground"),

            HEADERBAR_BACKDROP_COLOR: get_color(&map, "headerbar", "backdrop"),
            HEADERBAR_BG_COLOR: get_color(&map, "headerbar", "background"),
            HEADERBAR_FG_COLOR: get_color(&map, "headerbar", "foreground"),
            HEADERBAR_BORDER_COLOR: get_color(&map, "headerbar", "border"),
            HEADERBAR_SHADE_COLOR: get_color(&map, "headerbar", "shade"),

            VIEW_BG_COLOR: get_color(&map, "view", "background"),
            VIEW_FG_COLOR: get_color(&map, "view", "foreground"),

            WINDOW_BG_COLOR: get_color(&map, "window", "background"),
            WINDOW_FG_COLOR: get_color(&map, "window", "foreground"),

            CARD_BG_COLOR: get_color(&map, "card", "background"),
            CARD_FG_COLOR: get_color(&map, "card", "foreground"),
            CARD_SHADE_COLOR: get_color(&map, "card", "shade"),

            DIALOG_BG_COLOR: get_color(&map, "dialog", "background"),
            DIALOG_FG_COLOR: get_color(&map, "dialog", "foreground"),

            POPOVER_FG_COLOR: get_color(&map, "popover", "foreground"),
            POPOVER_BG_COLOR: get_color(&map, "popover", "background"),
        },
    ))
}
