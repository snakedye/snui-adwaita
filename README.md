# snui-adwaita

A [snui](https://gitlab.com/snakedye/snui) library implementing [GNOME's Human interface guidelines](https://developer.gnome.org/hig/index.html).

### Demo

```sh
cargo run --example demo
```

![Welcome](./sc/dark.png)
![Demo](./sc/light.png)
